package main

import (
	"fmt"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"bitbucket.com/shareware/httpdaemon/api"
)

const (
	PIDFile     = "/tmp/daemonize.pid"
	logFileName = "httpdaemon.log"
)

var (
	logFile *os.File
	Logger  *log.Logger
)

func init() {
	var err error
	logFile, err = os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		log.Fatalf("Не удалось создать или открыть файл логов httpdaemon.log: %v\n", err)
	}
	multi := io.MultiWriter(os.Stdout, logFile)
	Logger = log.New(multi, "", log.Ldate|log.Ltime|log.Lshortfile)

}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage : %s [start|stop] \n ", os.Args[0])
		return
	}
	switch strings.ToLower(os.Args[1]) {
	case "start":
		start()
	case "stop":
		stop()
	case "main":
		daemonMain()
	default:
		Logger.Printf("Unknown command : %v\n", os.Args[1])
		Logger.Printf("Usage : %s [start|stop]\n", os.Args[0]) // return the program name back to %s
		closeLogger()
	}
}

func start() {
	if _, err := os.Stat(PIDFile); err == nil {
		Logger.Printf("Процесс уже запущен или файл существует /tmp/daemonize.pid.")
		return
	}
	cmd := exec.Command(os.Args[0], "main")
	if err := cmd.Start(); err != nil {
		Logger.Printf("Ошибка при запуске: %v\n", err)
		return
	}
	Logger.Printf("Daemon PID: %v\n", cmd.Process.Pid)
	if err := savePID(cmd.Process.Pid); err != nil {
		Logger.Printf("Не удалось сохранить или создать pid файл /tmp/daemonize.pid : %v\n", err)
		cmd.Process.Signal(syscall.SIGTERM)
	}
}

func stop() {
	Logger.Println("Инициирован процесс остановки приложения.")

	if _, err := os.Stat(PIDFile); err == nil {
		data, err := ioutil.ReadFile(PIDFile)
		if err != nil {
			Logger.Println("Приложение не запущено.")
			return
		}
		ProcessID, err := strconv.Atoi(string(data))

		if err != nil {
			Logger.Printf("Ошибка при чтении из PID файла %v.\n", PIDFile)
			removePID()
			return
		}

		process, err := os.FindProcess(ProcessID)

		if err != nil {
			Logger.Printf("Не удалось найти процесс с PID [%v]: %v.\n", ProcessID, err)
			removePID()
			return
		}
		//// remove PID file
		//os.Remove(PIDFile)

		Logger.Printf("Остановка процесса с ID [%v].\n", ProcessID)
		// kill process and exit immediately
		err = process.Signal(os.Interrupt)
		if err != nil {
			Logger.Printf("Не удалось остановить процесс с ID [%v]: %v \n", ProcessID, err)
			return
		}

	} else {
		Logger.Println("Приложение не запущено.")
	}
}

func daemonMain() {
	go gracefulShutdown()
	store, err := createStore()
	if err != nil {
		Logger.Println(err)
		closeApp()
	}
	mux := http.NewServeMux()
	s := &api.Service{
		Storage: store,
		Logger:  Logger,
	}
	mux.HandleFunc("/download", s.Download())
	mux.HandleFunc("/upload", s.Upload(&api.FileZip{}))
	mux.HandleFunc("/delete", s.Delete())
	Logger.Printf("Service listen port: %v\n", ":8080")
	err = http.ListenAndServe(":8080", mux)
	if err != nil {
		Logger.Printf("Ошибка при запуске сервера: %v\n", err)
	}
}

func createStore() (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		return "", errors.New("Ошибка при определении текущего каталога: " + err.Error())
	}
	if _, err = os.Stat(dir + "/store"); err != nil {
		err = os.Mkdir(dir+"/store", os.ModePerm)
		if err != nil {
			return "", errors.New("Ошибка при создании каталога хранилища: " + err.Error())
		}
	}
	return dir + "/store", nil
}

func closeApp() {
	removePID()
	closeLogger()
	os.Exit(0)
}

func closeLogger() {
	if err := logFile.Close(); err != nil {
		Logger.Printf("Ошибка при закрытии файла логов: %v\n", err)
	}
}

//gracefulShutdown Удаление pid файла перед выходом из программы
func gracefulShutdown() {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch)
	signalType := <-ch
	signal.Stop(ch)
	Logger.Printf("Получен сигнал выхода из программы: %v\n", signalType)
	closeApp()
}

//removePID удаление файла с PID процесса
func removePID() {
	if _, err := os.Stat(PIDFile); err == nil {
		if err := os.Remove(PIDFile); err != nil {
			Logger.Printf("Ошибка при удалении PID файла: %v\n", err)
		}
	}
}

//savePID сохранение файла с PID процесса
func savePID(pid int) error {
	file, err := os.Create(PIDFile)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.WriteString(strconv.Itoa(pid))

	if err != nil {
		return err
	}
	if err := file.Sync(); err != nil {
		return err
	}
	return nil
}
