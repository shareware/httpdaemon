package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"testing"
)

func Test_createStore(t *testing.T) {
	want,err:=os.Getwd()
	if err!=nil{
		t.Errorf("error when preare to test: %v\n",err)
		return
	}
	want+="/store"
	got,err:=createStore()
	if err!=nil{
		t.Errorf("createstore() got = %v, want =%v\n",err,nil)
		return
	}
	if got != want{
		t.Errorf("createStore() got path = %v, want path = %v\n",got,want)
	}
}



func Test_savePID(t *testing.T) {
	pid :=os.Getpid()
	if _,err:=os.Stat(PIDFile);err==nil{
		err=os.Remove(PIDFile)
		if err!=nil{
			t.Errorf("Error when prepare to test:%v\n",err)
			return
		}
	}

	if err:=savePID(pid);err!=nil{
		t.Errorf("savePID() got error = %v, want error %v\n", err, nil)
		return
	}
	p,err:=ioutil.ReadFile(PIDFile)
	if err!=nil{
		t.Errorf("Error when read PIDFile: %v\n",err)
		return
	}
	if string(p)!=strconv.Itoa(pid){
		t.Errorf("PID in file got=%s, want=%v\n",p,pid)

	}
}



