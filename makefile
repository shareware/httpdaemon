dbuild:
	docker build . -t httpdaemon
drun:
	docker run -p 8080:8080 -it httpdaemon /bin/bash
build:
	go build .
run:
	./httpdaemon start
stop:
	./httpdaemon stop