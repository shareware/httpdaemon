package api

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

type ApiService interface {
	Download() http.HandlerFunc
	Upload(fh FileHandler) http.HandlerFunc
	Delete() http.HandlerFunc
}

type FileHandler interface{
	Pre() error
	Post(filepath string) error
}

type Service struct {
	Storage string
	Logger  *log.Logger
}


//Download возвращает хэндлер который обрабатывает запрос на отправку файла.
//Валидируется полученый url, хэш файла, наличие файла в хранилище.
func (s *Service) Download() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if keys, ok := r.URL.Query()["hash"]; ok && len(keys) > 0 && isValid(keys[0]) {
			s.Logger.Printf("Получени запрос на загрузку файла: %s.\n", keys[0])
			if exist,path:=hashExist(s.Storage, keys[0]);exist {
				fl, err := os.OpenFile(path, os.O_RDWR, os.ModePerm)
				defer fl.Close()
				if err != nil {
					s.Logger.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				var flName string
				info,err:=fl.Stat()
				if err!=nil{
					s.Logger.Println(err)
					flName = keys[0]
				}
				flName = info.Name()
				w.Header().Set("Content-Disposition", "attachment; filename="+flName)
				w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
				_, err = io.Copy(w, fl)
				if err != nil {
					s.Logger.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
				}
				return
			}
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("name не вадилный"))
		}
	}
}

//Upload возвращает хэндлер который обрабатывает запрос на загрузку файла.
//Валидируется наличие в хранилище.
func (s *Service) Upload(fh FileHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file, _, err := r.FormFile("file")
		defer file.Close()
		if err != nil {
			s.Logger.Println(err)
		}
		//if header.Size == 0 {
		//	w.WriteHeader(http.StatusBadRequest)
		//	fmt.Fprintf(w, "Загружаемый файл пустой.")
		//	return
		//}
		filesrc, err := ioutil.ReadAll(file)
		if err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		hash := fmt.Sprintf("%x", md5.Sum(filesrc))
		if !isValid(hash) {
			w.WriteHeader(http.StatusInternalServerError)
			s.Logger.Printf("Не валидный хэш файла name =%s, len = %v\n", hash, len(hash))
			return
		}
		if exist,_:= hashExist(s.Storage, hash);exist {
			w.Header().Add("hash", hash)
			w.WriteHeader(http.StatusCreated)
			fmt.Fprintf(w, "Файл уже существует.")
			return
		}
		var path string
		if path, err = createPathToFile(s.Storage, hash); err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		fl, err := os.Create(path)
		defer fl.Close()
		if err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		size, err := io.Copy(fl, bytes.NewReader(filesrc))
		if err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		s.Logger.Printf("Загружается файл: %s\nРазмер: %vb\n", hash, size)
		w.Header().Add("hash", hash)
		w.WriteHeader(http.StatusOK)
		if fh!=nil{
			if exist,path:=hashExist(s.Storage,hash);exist{
				err:=fh.Post(path)
				if err!=nil{
					s.Logger.Println(err)
				}
			}
		}
	}
}
//Delete возвращает хэндлер который обрабатывает запрос.
//Валидируется url,хэш
func (s *Service) Delete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if keys, ok := r.URL.Query()["hash"]; ok && len(keys) > 0 && isValid(keys[0]) {
			s.Logger.Printf("Получени запрос на удаление файла: %s.\n", keys[0])
			if exist,_:=hashExist(s.Storage, keys[0]);exist {
				if err := delete(s.Storage, keys[0]); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					s.Logger.Println(err)
					return
				}
				s.Logger.Printf("Удален: %s/%s.\n", hashDir(s.Storage, keys[0]), keys[0])
				w.WriteHeader(http.StatusOK)
			} else {
				s.Logger.Println("Файл не найден.")
				w.WriteHeader(http.StatusNotFound)
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			_, err := w.Write([]byte("Не верно заданы параметры в url или name, требуется name.Size=" + strconv.Itoa(md5.Size)))
			if err != nil {
				s.Logger.Println(err)
			}
		}
	}
}

func delete(storage, hash string) error {
	err := os.Remove(hashDir(storage, hash) + "/" + hash)
	if err != nil {
		return err
	}
	//удаляю каталог если он пустой
	if files, err := ioutil.ReadDir(hashDir(storage, hash)); err == nil && len(files) == 0 {
		if err := os.Remove(hashDir(storage, hash)); err != nil {
			return err
		}
	}
	return nil
}

func createPathToFile(storage, hash string) (string, error) {
	if _, err := os.Stat(hashDir(storage, hash)); err != nil {
		err := os.MkdirAll(hashDir(storage, hash), os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return hashDir(storage, hash) + "/" + hash, nil
}
//hashExist проверяет существует ли hash в хранилище storage
func hashExist(storage, hash string)(bool,string)  {

	if _, err := os.Stat(fmt.Sprintf("%s/%s", hashDir(storage, hash), hash)); err == nil {
		return true,fmt.Sprintf("%s/%s", hashDir(storage, hash), hash)
	}else{
		if _, err := os.Stat(fmt.Sprintf("%s/%s.zip", hashDir(storage, hash), hash)); err == nil {
			return true,fmt.Sprintf("%s/%s.zip", hashDir(storage, hash), hash)
		}
	}
	return false,""
}

//hashDir возвращает путь до каталога на основе хэша
func hashDir(storage, hash string) string {
	return fmt.Sprintf("%s/%s", storage, hash[:2])
}

func isValid(hash string) bool {
	if len(hash) != md5.Size*2 {
		return false
	}
	return true
}
