package api

import (
	"os"
	"testing"
)

func TestFileZip_Post(t *testing.T) {
	err:=MakeStore(TestService.Storage)
	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(TestService.Storage)

	type test struct {
		name    string
		filePath string
		wantErr bool
	}

	tests := []test{}

	for _,f:=range StoreFiles{
		tests = append(tests,test{"",f.fullPath,false})
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := FileZip{}
			if err := f.Post(tt.filePath); (err != nil) != tt.wantErr {
				t.Errorf("Post() error = %v, wantErr %v", err, tt.wantErr)
			}
			if _,err:=os.Stat(tt.filePath+".zip");err!=nil{
				t.Errorf("Post() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
