package api

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

type FlStore struct{
	name     string
	content  []byte
	fullPath string
}

var(
	TestService = &Service{
		Storage: "",
		Logger:  log.New(os.Stdout,"",log.Ldate|log.Ltime|log.Lshortfile),
	}
	StoreFiles =[]FlStore{
		{"notEmpty",
			[]byte("HelloWorld"),
			"",
		},
		{
			"empty",
			[]byte(""),
			"",
		},
		{
			"notEmpt.zip",
			[]byte("123123"),
			"",
		},
	}
)

func TestService_Delete(t *testing.T) {
	var err error
	TestService.Storage,err= getStorePath()
	if err!=nil{
		t.Fatal(err)
	}
	err= MakeStore(TestService.Storage)
	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(TestService.Storage)
	tests:=[]struct{
		name string
		method string
		url string
		expectCode int
	}{
		{
			"Bad Request",
			"GET",
			"/delete?hash=1234",
			http.StatusBadRequest,
		},
		{
			"Bad Request",
			"GET",
			"/delete?hash=",
			http.StatusBadRequest,
		},
		{
			"Bad Request",
			"GET",
			"/delete?ha=",
			http.StatusBadRequest,
		},
		{
			"Not Found",
			"GET",
			"/delete?hash=12345678912345671234567891234567",
			http.StatusNotFound,
		},
		{
			"Ok",
			"GET",
			"/delete?hash="+ StoreFiles[0].name,
			http.StatusOK,
		},
	}


	for _,tt:=range tests{
		t.Run(tt.name,func(t *testing.T){
			req,err:=http.NewRequest(tt.method,tt.url,nil)
			if err!=nil{
				t.Fatal(err)
			}
			rr:=httptest.NewRecorder()
			handler:=TestService.Delete()

			handler.ServeHTTP(rr,req)

			if rr.Code!=tt.expectCode{
				t.Fatalf("Delete() Handler code got=%v, want=%v",rr.Code,tt.expectCode)
			}
		})
	}
}

func TestService_Download(t *testing.T) {
	var err error
	TestService.Storage,err= getStorePath()

	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(TestService.Storage)
	err = MakeStore(TestService.Storage)
	if err!=nil{
		t.Fatal(err)
	}

	tests:=[]struct{
		name string
		method string
		url string
		expectCode int
	}{
		{
			"Bad Request",
			"GET",
			"/download?hash=1234",
			http.StatusBadRequest,
		},
		{
			"Bad Request",
			"GET",
			"/download?hash=",
			http.StatusBadRequest,
		},
		{
			"Bad Request",
			"GET",
			"/download?ha=",
			http.StatusBadRequest,
		},
		{
			"Not Found",
			"GET",
			"/download?hash=12345678912345671234567891234567",
			http.StatusNotFound,
		},
		{
			"Ok",
			"GET",
			"/download?hash="+ StoreFiles[0].name,
			http.StatusOK,
		},
	}

	for _,tt:=range tests{
		t.Run(tt.name, func(t *testing.T) {
			req,err:=http.NewRequest(tt.method,tt.url,nil)
			if err!=nil{
				t.Fatal(err)
			}
			rr:=httptest.NewRecorder()
			handler := TestService.Download()
			handler.ServeHTTP(rr,req)
			if rr.Code!=tt.expectCode{
				t.Fatalf("Download() Handler code got=%v, want=%v",rr.Code,tt.expectCode)
			}
		})
	}


}

func TestService_Upload(t *testing.T) {
	var err error
	TestService.Storage,err= getStorePath()
	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(TestService.Storage)
	err= MakeStore(TestService.Storage)
	if err!=nil{
		t.Fatal(err)
	}

	fl,err:=os.Create(TestService.Storage+"/newNotEmptyFile")
	if err!=nil{
		t.Fatal(err)
	}
	_,err=fl.WriteString("123")
	if err!=nil{
		t.Fatal(err)
	}
	err=fl.Close()
	if err!=nil{
		t.Fatal(err)
	}

	hashNewNotEmptyFile := fmt.Sprintf("%x",md5.Sum([]byte("123")) )



	tests:=[]struct{
		name       string
		method     string
		url        string
		expectCode int
		file       FlStore
		expectHash string
	}{
		{
			"Bad Request",
			"POST",
			"/upload",
			http.StatusCreated,
			StoreFiles[1],
			StoreFiles[1].name,

		},
		{
			"Ok",
			"POST",
			"/upload",
			http.StatusOK,
			FlStore{
				name:     hashNewNotEmptyFile,
				content:  []byte("123"),
				fullPath: TestService.Storage+"/"+hashNewNotEmptyFile,
			},
			hashNewNotEmptyFile,

		},
		{
			"Created",
			"POST",
			"/upload",
			http.StatusCreated,
			StoreFiles[0],
			StoreFiles[0].name,

		},
	}

	for _,tt:=range tests{
		t.Run(tt.name, func(t *testing.T) {
			body := new(bytes.Buffer)
			writer := multipart.NewWriter(body)
			part, err := writer.CreateFormFile("file", tt.file.fullPath)
			if err != nil {
				t.Fatal(err)
			}
			_, err = part.Write(tt.file.content)
			if err != nil {
				t.Fatal(err)

			}
			err = writer.Close()
			if err != nil {
				t.Fatal(err)
			}
			req, _ := http.NewRequest("POST", tt.url, body)
			req.Header.Add("Content-Type", writer.FormDataContentType())
			rr:=httptest.NewRecorder()
			handler:= TestService.Upload(nil)
			handler.ServeHTTP(rr,req)
			if rr.Code!=tt.expectCode{
				t.Fatalf("Upload() Handler code got=%v, want=%v",rr.Code,tt.expectCode)
			}
			if rr.Header().Get("hash")!=tt.expectHash{
				t.Fatalf("Upload() Handler hash got=%v, want=%v",rr.Header().Get("hash"),tt.expectHash)
			}
		})
	}
}



func Test_createPathToFile(t *testing.T) {
	store,err:= getStorePath()
	defer RemoveStore(store)
	if err!=nil{
		t.Fatal(err)
	}
	type args struct {
		storage string
		hash    string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr error
	}{
		{
			"Create Path",
			args{store,"12345678912345671234567891234567"},
			store+"/12/12345678912345671234567891234567",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := createPathToFile(tt.args.storage, tt.args.hash)
			if err != tt.wantErr {
				t.Fatalf("createPathToFile() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got != tt.want {
				t.Fatalf("createPathToFile() got = %v, want %v", got, tt.want)
			}
			if _,err := os.Stat(tt.args.storage+"/"+tt.args.hash[:2]);err!=nil{
				t.Fatalf("Path not created! %v",tt.args.storage+"/"+tt.args.hash[:2])
			}
		})
	}
}


func Test_hashExist(t *testing.T) {
	store,err:= getStorePath()
	if err!=nil{
		t.Fatal(err)
	}
	err = MakeStore(store)
	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(store)

	type test struct{
		name    string
		hash    string
		exist bool
		path string
	}

	var tests []test

	tests = append(tests,test{"Exist", StoreFiles[0].name,true, StoreFiles[0].fullPath})
	tests = append(tests,test{"Not Exist","12345678912345671234567891234567",false,""})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			exist,path:=hashExist(store, tt.hash)
			if exist!=tt.exist || path!=tt.path {
				t.Fatalf("hashExist() got exist=%v path=%v, want exist=%v,path=%v", exist, path,tt.exist,tt.path)
			}
		})
	}
}

func Test_isValid(t *testing.T) {
	tests := []struct {
		name string
		hash string
		want bool
	}{
		{
			"Valid Hash",
			"12345678912345671234567891234567",
			true,
		},
		{
			"Not Valid Hash",
			"12345678912345671234567891",
			false,
		},
		{
			"Not Valid Hash",
			"123456789123456712345678912345677777",
			false,
		},
		{
			"Not Valid Hash",
			"",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValid(tt.hash); got != tt.want {
				t.Fatalf("isValid() = %v, want %v", got, tt.want)
			}
		})
	}
}



func Test_delete(t *testing.T) {
	store,err:= getStorePath()
	if err!=nil{
		t.Fatal(err)
	}
	err= MakeStore(store)
	if err!=nil{
		t.Fatal(err)
	}
	defer RemoveStore(store)
	type test struct{
		name                string
		file                FlStore
		dirExistAfterDelete bool
	}

	var tests []test

	for i:=0;i<len(StoreFiles);i++{
		if i!=len(StoreFiles)-1{
			tests = append(tests,test{"Not Last File", StoreFiles[i],true})
		}else{
			tests =  append(tests,test{"Last File", StoreFiles[i],false})
		}
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := delete(store, tt.file.name); err != nil {
				t.Fatalf("delete() error = %v", err)
			}
			if _,err=os.Stat(tt.file.fullPath);err==nil{
				t.Fatal("delete() file not deleted but should")
			}
			_,err= os.Stat(store+"/"+tt.file.name[:2])
			if tt.name == "Remove First File" && err!=nil{
				t.Fatalf("delete() dirExistAfterDelete got=%v, want=%v",false,tt.dirExistAfterDelete)

			}
			if tt.name == "Remove Last File" && err==nil{
				t.Fatalf("delete() dirExistAfterDelete got=%v, want=%v",true,tt.dirExistAfterDelete)
			}
		})
	}

}

func getStorePath() (string,error){
	store,err:=os.Getwd()
	if err!=nil{
		return "",err
	}
	store+="/Teststore"
	return store,nil
}

func MakeStore(store string) error{
	if _,err:=os.Stat(store);err==nil{
		return errors.New("Store already exist. Should RemoveStore() after test.")
	}
	err:=os.MkdirAll(store+"/tmp",os.ModePerm)
	if err!=nil{
		return err
	}
	for i:=0;i<len(StoreFiles);i++{
		fl,err:=os.Create(store+"/tmp/" +StoreFiles[i].name)
		if err!=nil{
			return err
		}
		_,err=fl.Write(StoreFiles[i].content)
		if err!=nil{
			return err
		}
		err=fl.Close()
		if err!=nil{
			return err
		}
		hash:=fmt.Sprintf("%x",md5.Sum(StoreFiles[i].content))
		StoreFiles[i].name = hash
		err=os.MkdirAll(store+"/"+hash[:2],os.ModePerm)
		if err!=nil{
			return err
		}
		StoreFiles[i].fullPath = store+"/"+hash[:2]+"/"+hash
		hashFl,err:=os.Create(StoreFiles[i].fullPath)

		if err!=nil{
			return err
		}
		_,err=io.Copy(hashFl,bytes.NewReader(StoreFiles[i].content))
		if err!=nil{
			return err
		}
		StoreFiles[i].name = hash
	}
	err=os.RemoveAll(store+"/tmp")
	if err!=nil{
		return err
	}
	return nil
}

func RemoveStore(path string){
	err:=os.RemoveAll(path)
	if err!=nil {
		log.Printf("error when complete test: %v\n", err)
	}
}