package api

import (
	"archive/zip"
	"io"
	"os"
)



type FileZip struct{
}

func (f FileZip) Pre() error{
	return nil
}

func (f FileZip) Post(filePath string) error{
	if _,err:=os.Stat(filePath);err!=nil{
		return err
	}
	err:=zipFile(filePath)
	if err!=nil{
		if _,err:=os.Stat(filePath +".zip");err==nil{
			if err:=os.Remove(filePath +".zip");err!=nil{
				return err
			}
		}
		return err
	}
	err=os.Remove(filePath)
	if err!=nil{
		return err
	}
	return nil
}

func zipFile(filename string) error {

	newZipFile, err := os.Create( filename+".zip")
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

		if err = addFileToZip(zipWriter, filename); err != nil {
			return err
		}

	return nil
}

func addFileToZip(zipWriter *zip.Writer, filename string) error {
	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fileToZip.Close()
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}
	//header.Name = strings.Split(filename,"/")[len(strings.Split(filename,"/"))-1]
	header.Method = zip.Deflate
	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}

