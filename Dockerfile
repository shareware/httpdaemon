FROM golang:1.12
WORKDIR /bitbucket.com/shareware/httpdaemon
COPY . .
RUN go install ./...
